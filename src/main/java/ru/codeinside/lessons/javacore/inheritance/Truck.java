package ru.codeinside.lessons.javacore.inheritance;

import ru.codeinside.lessons.javacore.inheritance.enums.VehicleColor;

public class Truck extends Vehicle {
    public boolean isCanvasBack;

    public Truck(int year, String color, boolean canvas) {
        super(year, color);
        setCanvasBack(canvas);
    }

    public Truck(int year, VehicleColor color, boolean canvas) {
        super(year, color);
        setCanvasBack(canvas);
    }

    @Override
    public String getVehicleInfo() {
        return String.format("VIN: %1s\nProduction year: %2s\nColor: %3s\nIs back canvas?: %b",
                this.vin,
                this.yearOfProduction,
                this.color,
                this.isCanvasBack
        );
    }

    public void setCanvasBack(boolean check) {
        this.isCanvasBack = check;
    }

    public boolean isCanvasBack() {
        return this.isCanvasBack;
    }
}
