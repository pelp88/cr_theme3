package ru.codeinside.lessons.javacore.implementation;

public class Freelancer implements Service {
    String nickname;
    String wallet;

    @Override
    public String makeService() {
        return "Freelancer created";
    }

    @Override
    public String getName() {
        return this.nickname;
    }

    @Override
    public String getShortName() {
        return Service.super.getShortName();
    }
}
