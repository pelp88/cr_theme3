package ru.codeinside.lessons.javacore.implementation;

interface Service {
    String makeService();
    String getName();
    default String getShortName() {
        return "НЕТ ИНФОРМАЦИИ";
    }
}
